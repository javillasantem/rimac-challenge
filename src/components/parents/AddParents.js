import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import { useHistory } from "react-router-dom";

export const AddParents = () => {
    const history = useHistory();
    const routeChange = () =>{
        let path = 'select-plan';
        history.push(path);
    }

    const [users, usersSet] = React.useState([]);

    useEffect(() => {
        fetch("https://randomuser.me/api")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    usersSet(result.results);
                },
                (error) => {
                    console.log(error);
                }
            )
    }, []);
    
    console.log(users[0]);
    
    return (
        <>
            <div id="page__parents">
                <div className="content__background"></div>

                <div className="content__main">
                    <div className="content__logo">
                        <div className="logo__container">
                            <img
                                src="/assets/images/logo-rimac.png"
                                title="Rimac Seguros" />
                        </div>
                    </div>

                    <div className="content__info">
                        <span className="info__title">Seguro de salud</span>
                        <span className="info__item_mobile">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Cómpralo de manera fácil y rápida</span>
                        <span className="info__item">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Hasta S/. 12 millones de cobertura anual</span>
                        <span className="info__item">Más de 300 clínicas en todo el Perú</span>
                    </div>

                    <div className="content__illustration">
                        <img
                            className="illustration__desktop"
                            src="/assets/images/illustration_desktop.png"
                            title="Rimac Seguros" />

                        <img
                            className="illustration__mobile"
                            src="/assets/images/illustration_mobile.png"
                            title="Rimac Seguros" />
                    </div>

                    <div className="content__form">
                        <div>
                            <Box
                                className="form__title"
                                display="inline">
                                Hola,
                                <Box
                                    className="form__title_red"
                                    display="inline"> Luisa
                                </Box>
                            </Box>
                        </div>

                        <div>
                            <Box className="form__subtitle">Ingresa los datos para comenzar.</Box>
                        </div>

                        <div className="form__documentos">
                            <FormControl
                                className="form__documentotipo"
                                variant="outlined">
                                <Select
                                    display="inline"
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined">
                                    <MenuItem value={10}>DNI</MenuItem>
                                    <MenuItem value={20}>CEX</MenuItem>
                                </Select>
                            </FormControl>

                            <TextField
                                className="form__documentonumero"
                                display="inline"
                                id="standard-basic"
                                label="Nro. de Documento"
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <TextField
                                fullWidth="true"
                                id="parents_nombres"
                                label="Nombres"
                                value={users[0].name.first}
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <TextField
                                fullWidth="true"
                                id="parents_apellido_paterno"
                                label="Apellido Paterno"
                                value={users[0].name.last}
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <TextField
                                fullWidth="true"
                                id="parents_apellido_materno"
                                label="Apellido Materno"
                                value={users[0].name.last}
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <TextField
                                id="date"
                                fullWidth="true"
                                label="Fecha de nacimiento"
                                type="date"
                                defaultValue="2017-05-24"
                                InputLabelProps={{
                                    shrink: true,
                                }} />
                        </div>
                        
                        <div className="form__item">
                            <FormControl component="fieldset">
                                <FormLabel component="legend" className="form__asegurar_title">Género</FormLabel>
                                <RadioGroup aria-label="gender" name="genero">
                                    <FormControlLabel className="form__asegurar_item" value="masculino" control={<Radio />} label="Masculino" />
                                    <FormControlLabel className="form__asegurar_item" value="femenino" control={<Radio />} label="Femenino" />
                                </RadioGroup>
                            </FormControl>
                        </div>

                        <div className="form__item">
                            <FormControl component="fieldset">
                                <FormLabel component="legend" className="form__asegurar_title">¿A quién vamos a asegurar?</FormLabel>
                                <RadioGroup aria-label="gender" name="asegurados">
                                    <FormControlLabel className="form__asegurar_item" value="solo_yo" control={<Radio />} label="Solo a mí" />
                                    <FormControlLabel className="form__asegurar_item" value="yo_mi_familia" control={<Radio />} label="A mí y a mi familia" />
                                </RadioGroup>
                            </FormControl>
                        </div>

                        <div className="form__item">
                            <Button
                                className="form__comencemos"
                                onClick={routeChange} >
                                CONTINUAR
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
