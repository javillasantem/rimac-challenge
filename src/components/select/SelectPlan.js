import React from 'react'
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
/* import ExpandMoreIcon from '@material-ui/icons/ExpandMoreIcon'; */
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useHistory } from "react-router-dom";

export const SelectPlan = () => {
    const history = useHistory();
    const routeChange = () =>{
        let path = 'confirm';
        history.push(path);
    }

    return (
        <>
            <div id="page__plans">
                <div className="content__background"></div>

                <div className="content__main">
                    <div className="content__logo">
                        <div className="logo__container">
                            <img
                                src="/assets/images/logo-rimac.png"
                                title="Rimac Seguros" />
                        </div>
                    </div>

                    <div className="content__info">
                        <span className="info__title">Seguro de salud</span>
                        <span className="info__item_mobile">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Cómpralo de manera fácil y rápida</span>
                        <span className="info__item">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Hasta S/. 12 millones de cobertura anual</span>
                        <span className="info__item">Más de 300 clínicas en todo el Perú</span>
                    </div>

                    <div className="content__illustration">
                        <img
                            className="illustration__desktop"
                            src="/assets/images/illustration_desktop.png"
                            title="Rimac Seguros" />

                        <img
                            className="illustration__mobile"
                            src="/assets/images/illustration_mobile.png"
                            title="Rimac Seguros" />
                    </div>

                    <div className="content__form">
                        <div>
                            <Box
                                className="form__title"
                                display="inline">
                                Elige,
                                <Box
                                    className="form__title_red"
                                    display="inline"> tu protección
                                </Box>
                            </Box>
                        </div>

                        <div>
                            <Box className="form__subtitle">Selecciona tu plan de salud ideal.</Box>
                        </div>

                        <div>
                            <Card>
                                <CardActionArea>
                                    <CardMedia
                                        image="/static/images/cards/contemplative-reptile.jpg"
                                        title="Contemplative Reptile" />

                                    <CardContent>
                                        <span>Cobertura máxima</span>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            S/ 1MM
                                        </Typography>
                                        <hr></hr>
                                        <ul>
                                            <li>Lima</li>
                                            <li>(zona de cobertura)</li>
                                            <li>+30 clínicas</li>
                                            <li>(en red afiliada)</li>
                                            <li>Médico a domicilio</li>
                                            <li>Chequeos preventivos</li>
                                            <li>Reembolso nacional</li>
                                            <li>Reembolso internacional</li>
                                        </ul>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </div>

                        <div className="form__item">
                            <Accordion>
                                <AccordionSummary
                                    aria-controls="panel1a-content"
                                    id="panel1a-header" >
                                    <Typography>Servicios brindados</Typography>
                                </AccordionSummary>

                                <AccordionDetails>
                                    <Typography>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                        sit amet blandit leo lobortis eget.
                                    </Typography>
                                </AccordionDetails>
                            </Accordion>

                            <Accordion>
                                <AccordionSummary
                                    aria-controls="panel2a-content"
                                    id="panel2a-header" >
                                    <Typography>Exclusiones</Typography>
                                </AccordionSummary>

                                <AccordionDetails>
                                    <Typography>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                        sit amet blandit leo lobortis eget.
                                    </Typography>
                                </AccordionDetails>
                            </Accordion>
                        </div>

                        <div className="form__item">
                            <Button
                                className="form__comencemos"
                                onClick={routeChange} >
                                COMPRAR PLAN
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
