import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import { useHistory } from "react-router-dom";

export const LoginScreen = () => {
    const history = useHistory();
    const routeChange = () =>{ 
        let path = 'add-parents';
        history.push(path);
    }

    const [users, usersSet] = React.useState([]);

    useEffect(() => {
        fetch("https://randomuser.me/api")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    usersSet(result.results);
                },
                (error) => {
                    console.log(error);
                }
            )
    }, []);

    return (
        <>
            <div id="page__login">
                <div className="content__background"></div>

                <div className="content__main">
                    <div className="content__logo">
                        <div className="logo__container">
                            <img
                                src="/assets/images/logo-rimac.png"
                                title="Rimac Seguros" />
                        </div>
                    </div>

                    <div className="content__info">
                        <span className="info__title">Seguro de salud</span>
                        <span className="info__item_mobile">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Cómpralo de manera fácil y rápida</span>
                        <span className="info__item">Cotiza y compra tu seguro 100% digital</span>
                        <span className="info__item">Hasta S/. 12 millones de cobertura anual</span>
                        <span className="info__item">Más de 300 clínicas en todo el Perú</span>
                    </div>

                    <div className="content__illustration">
                        <img
                            className="illustration__desktop"
                            src="/assets/images/illustration_desktop.png"
                            title="Rimac Seguros" />

                        <img
                            className="illustration__mobile"
                            src="/assets/images/illustration_mobile.png"
                            title="Rimac Seguros" />
                    </div>

                    <div className="content__form">
                        <div>
                            <Box
                                className="form__title"
                                display="inline">
                                Obtén tu
                                <Box
                                    className="form__title_red"
                                    display="inline"> seguro ahora
                                </Box>
                            </Box>
                        </div>

                        <div>
                            <Box className="form__subtitle">Ingresa los datos para comenzar.</Box>
                        </div>

                        <div className="form__documentos">
                            <FormControl
                                className="form__documentotipo"
                                variant="outlined">
                                <Select
                                    display="inline"
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined">
                                    <MenuItem value={10}>DNI</MenuItem>
                                    <MenuItem value={20}>CEX</MenuItem>
                                </Select>
                            </FormControl>

                            <TextField
                                className="form__documentonumero"
                                display="inline"
                                id="standard-basic"
                                label="Nro. de Documento"
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <TextField
                                id="date"
                                fullWidth="true"
                                label="Fecha de nacimiento"
                                type="date"
                                defaultValue="2017-05-24"
                                InputLabelProps={{
                                    shrink: true,
                                }} />
                        </div>
                        
                        <div className="form__item">
                            <TextField
                                fullWidth="true"
                                id="standard-basic"
                                label="Celular"
                                value={users[0].cell}
                                variant="outlined"/>
                        </div>

                        <div className="form__item">
                            <FormControlLabel
                                className="form__politicas"
                                control={<Checkbox name="checkedA" />}
                                label='Acepto la Política de Protección de Datos Personales y los Términos y Condiciones' />
                        </div>

                        <div className="form__item">
                            <FormControlLabel
                                className="form__politicas"
                                control={<Checkbox name="checkedB" />}
                                label="Acepto la Política de Envío de Comunicaciones Comerciales" />
                        </div>

                        <div className="form__item">
                            <Button
                                className="form__comencemos"
                                onClick={routeChange} >
                                COMENCEMOS
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
