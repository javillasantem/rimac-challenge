import React from 'react'

export const InitialScreen = () => {
    return (
        <>
            <section id="contentMain" class="first-page">
                <div class="bg-left"><hr></hr></div>

                <div class="container-update">
                    <div class="home__logo">
                        <div class="container">
                            <img src="/assets/images/logo-rimac.png" alt="Logo RIMAC Seguros" />
                        </div>
                    </div>

                    <div class="content-info main-title">
                        <h1 class="hide-tablet-on-up">Seguro de<b> salud</b></h1>
                        <ul>
                            <li>Cómpralo de manera fácil y rápida</li>
                            <li>Cotiza y compra tu seguro 100% digital</li>
                            <li>Hasta S/. 12 millones de cobertura anual</li>
                            <li>Más de 300 clínicas en todo el Perú</li>
                        </ul>
                        {/* <h1 class="hide-mobile">Actualiza tus datos,<b> ¡Y recibe tu Kit de Prevención en casa!</b></h1>
                        <p class="subtitle">¡Es muy fácil, rápido y seguro!</p>
                        <span class="textfooter">© 2020 RIMAC Seguros y Reaseguros.</span> */}
                    </div>

                    <div class="content-art">
                        <picture>
                            {/* <source srcset="/datos/static/media/doctor-mobile.35aa6e80.png" media="(max-width: 1023px)" />
                            <source srcset="/datos/static/media/doctor-desktop.661a3403.png" media="(min-width: 1024px" /> */}
                            <img src="/assets/images/illustration.png" alt="Rimac Seguros" />
                        </picture>
                    </div>

                    <div class="content-form">
                        <div className="col s12 title-form">
                            <h3>Obtén tu <b>seguro ahora</b></h3>
                            <p>Ingresa los datos para comenzar.</p>
                        </div>

                        <form class="col">
                            <div class="box-document">
                                <div class="col s4 input__select">
                                    <div class="input-field">
                                        <select name="documentType">
                                            <option value="DNI">DNI</option>
                                            <option value="CE">C.E</option>
                                            <option value="RUC">RUC</option>
                                            <option value="PASAPORTE">PASAPORTE</option>
                                        </select>
                                        <img class="iconselect" src="/datos/static/media/arrowselect.eeeaa128.svg" alt="Rimac Seguros" />
                                    </div>
                                </div>
                                <div class="col s8 input__doc bxinputs">
                                    <div class="input-field">
                                        <input id="NumDocumento" type="text" class="validate" maxlength="8" value="" />
                                        <label class="" for="NumDocumento" data-error="" data-success="">Número de documento</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 bxinputs">
                                <div class="input-field">
                                    <input id="fullname" type="text" class="" value="" />
                                    <label class="" for="fullname" data-error="" data-success="">Nombres y Apellidos</label>
                                </div>
                            </div>
                            <div class="col s12 bxinputs">
                                <div class="input-field">
                                    <input id="cellphone" type="text" class="" maxlength="9" value="" />
                                    <label class="" for="cellphone" data-error="" data-success="">Celular</label>
                                </div>
                            </div>
                            <div class="col s12 bxinputs">
                                <div class="input-field">
                                    <input id="email" type="email" class="" value="" />
                                    <label class="" for="email" data-error="" data-success="">Correo Electrónico</label>
                                </div>
                            </div>
                            <button class="btn__primary medium" disabled="">CONTINUAR</button>
                        </form>
                    </div>
                </div>

                <div class="container-fluid">
                    <section class="bxfooter-mb">
                        <div class="container"><span class="footertxt">© 2021 RIMAC Seguros y Reaseguros.</span></div>
                    </section>
                </div>
            </section>
        </>
    )
}
