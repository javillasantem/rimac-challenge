import React from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Box from '@material-ui/core/Box';
import { useHistory } from "react-router-dom";

export const Confirm = () => {
    const history = useHistory();
    const routeChange = () =>{
        let path = 'confirm';
        history.push(path);
    }

    return (
        <>
            <div id="page__confirm">
                <div className="content__background"></div>

                <div className="content__main">
                    <div className="content__logo">
                        <div className="logo__container">
                            <img
                                src="/assets/images/logo-rimac.png"
                                title="Rimac Seguros" />
                        </div>
                    </div>

                    <div className="content__illustration">
                        <img
                            className="illustration__desktop"
                            src="/assets/images/confirm_illustration_desktop.png"
                            title="Rimac Seguros" />

                        <img
                            className="illustration__mobile"
                            src="/assets/images/confirm_illustration_mobile.png"
                            title="Rimac Seguros" />
                    </div>

                    <div className="content__form">
                        <div>
                            <Box
                                className="form__title"
                                display="inline">
                                ¡Gracias por
                                <Box
                                    className="form__title_red"
                                    display="inline"> confiar en nosotros!
                                </Box>
                            </Box>
                        </div>

                        <div>
                            <Box className="form__subtitle">Queremos conocer mejor la salud de los asegurados. Un asesor se pondrá en contacto contigo en las siguientes 48 horas.</Box>
                        </div>

                        <div className="form__item">
                            <Button
                                className="form__comencemos"
                                onClick={routeChange} >
                                IR A SALUD RIMAC
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
