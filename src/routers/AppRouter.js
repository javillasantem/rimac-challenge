import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import { InitialScreen } from '../components/auth/InitialScreen';
import { LoginScreen } from '../components/auth/LoginScreen';
import { Confirm } from '../components/confirm/Confirm';
import { AddParents } from '../components/parents/AddParents';
import { SelectPlan } from '../components/select/SelectPlan';

export const AppRouter = () => {
    return (
        <Router>
            <div>
                <Switch>
                    <Route
                        exact
                        path="/confirm"
                        component={ Confirm } />

                    <Route
                        exact
                        path="/select-plan"
                        component={ SelectPlan } />

                    <Route
                        exact
                        path="/add-parents"
                        component={ AddParents } />

                    <Route
                        exact
                        path="/"
                        component={ LoginScreen } />

                    <Route
                        exact
                        path="/initial"
                        component={ InitialScreen } />

                    <Redirect to="/" />

                </Switch>
            </div>
        </Router>
    )
}
